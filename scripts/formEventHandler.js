function formEventHandler() {
  const form = document.getElementById("application-form");
  const errorMessage = document.getElementById("error-message");

  form.addEventListener("submit", function (event) {
    event.preventDefault();

    const emriDheMbiemri = document
      .getElementById("emriDheMbiemri")
      .value.trim();
    const email = document.getElementById("email").value.trim();
    const tel = document.getElementById("tel").value.trim();
    const select = document.getElementById("select").value;

    const namePattern = /^[a-zA-Z\s]+$/;
    if (!namePattern.test(emriDheMbiemri)) {
      alert("Emri dhe mbiemri duhet të përmbajnë vetëm shkronja dhe hapsira.");
      return;
    }

    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailPattern.test(email)) {
      alert("Ju lutemi, jepni një adresë email valide.");
      return;
    }

    const phonePattern = /^04[34589]\d{6}$/;

    if (!phonePattern.test(tel)) {
      alert("Ju lutemi, jepni numrin e saktë");
      return;
    }

    console.log("Name and Surname: " + emriDheMbiemri);
    console.log("Email: " + email);
    console.log("Number: " + tel);
    console.log("Program: " + select);
  });
}
